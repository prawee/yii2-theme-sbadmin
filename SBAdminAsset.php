<?php
/*
 * 2014-10-24
 * @author Prawee Wongsa <konkeanweb@gmail.com>
 * http://startbootstrap.com/template-overviews/sb-admin/
 */
namespace prawee\theme\sbadmin;

use yii\web\AssetBundle;

class SBAdminAsset extends AssetBundle {

    public $sourcePath = '@vendor/prawee/yii2-theme-sb-admin/src';
    public $baseUrl = '@web';
    public $css = [
        'css/sb-admin.css',
        'css/plugins/morris.css',
        'font-awesome-4.1.0/css/font-awesome.min.css',
    ];
    public $js = [
        'js/jquery-1.11.0.js',
        'js/plugins/morris/raphael.min.js',
        'js/plugins/morris/morris.min.js',
        'js/plugins/morris/morris-data.js',
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    public function init() {
        parent::init();
    }

}
